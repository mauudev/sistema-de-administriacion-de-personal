from django import forms
from personalmanager.models import Empleado,Cargo,Area


class EmpleadoForm(forms.ModelForm):
    class Meta:
        model = Empleado

class CargoForm(forms.ModelForm):
    class Meta:
        model = Cargo

class AreaForm(forms.ModelForm):
    class Meta:
        model = Area