from django.db import models
from django.utils import timezone


class Area(models.Model):
    area = models.CharField(max_length=100)

    def __str__(self):
        return self.area

class Cargo(models.Model):
    cargo = models.CharField(max_length=100)

    def __str__(self):
        return self.cargo

class Empleado(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    Cargo = models.ForeignKey(Cargo)
    Area = models.ForeignKey(Area)
    direccion = models.CharField(max_length=200)
    telefono = models.CharField(max_length=10)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre




