from django.shortcuts import render
from django.shortcuts import get_object_or_404, render_to_response, redirect
from personalmanager.models import Empleado, Area, Cargo
from django.template import RequestContext  #desde aqui para trabajar con formularios
from personalmanager.form import EmpleadoForm,CargoForm,AreaForm
from django.http import HttpResponseNotFound
from django.contrib.auth.decorators import login_required
from django.utils import timezone



def homepage(request):
    return render_to_response('homepage.html', context_instance=RequestContext(request))

def empleado_index(request):
  #  empleados = Empleado.objects.filter(activo = False)
    empleados = Empleado.objects.select_related()
    cargos = Cargo.objects.all()
    return render_to_response('empleado_templates/empleado_index.html',{'empleados':empleados}, context_instance=RequestContext(request))# dict(empleados=empleados, cargos=cargos))

def cargo_index(request):
    cargos = Cargo.objects.all()
    return render_to_response('cargo_templates/cargo_index.html', {'cargos': cargos}, context_instance=RequestContext(request))

def area_index(request):
    areas = Area.objects.all()
    return render_to_response('area_templates/area_index.html', {'areas': areas}, context_instance=RequestContext(request))


@login_required
def empleado_crear(request):
    if request.method == 'POST': #desde aca va para la persistencia de datos
        form = EmpleadoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('empleados')
    else:
        form = EmpleadoForm()
    return render_to_response('empleado_templates/empleado_crear.html',
                                {'form': form},
                                context_instance=RequestContext(request))


@login_required
def empleado_editar(request, empleado_id):
    empleado = get_object_or_404(Empleado, pk=empleado_id)
    if request.method == 'POST':
        form = EmpleadoForm(request.POST,instance=empleado)
        if form.is_valid():
            form.save()
            #empleados = Empleado.objects.filter(activo = True)
            empleados = Empleado.objects.select_related()
            return render_to_response('empleado_templates/empleado_index.html',{'empleados':empleados}, context_instance=RequestContext(request))
           # return redirect('empleado_index',empleados)
    else:
        form = EmpleadoForm(instance=empleado)
    return render_to_response('empleado_templates/empleado_editar.html',{'form':form},
                                  context_instance = RequestContext(request))


@login_required
def empleado_eliminar(request, empleado_id):
    try:
          empleado = get_object_or_404(Empleado, pk=empleado_id)
          empleado.delete()
          empleados = Empleado.objects.select_related()
          return render_to_response('empleado_templates/empleado_index.html',{'empleados':empleados}, context_instance=RequestContext(request))
    except:
          return HttpResponseNotFound('<h1>Page not found</h1>')


@login_required
def cargo_crear(request):
    if request.method == 'POST': #desde aca va para la persistencia de datos
        form = CargoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('cargos')
    else:
        form = CargoForm()
    return render_to_response('cargo_templates/cargo_crear.html',
                                {'form': form},
                                context_instance=RequestContext(request))


@login_required
def cargo_editar(request, cargo_id):
    cargo = get_object_or_404(Cargo, pk=cargo_id)
    if request.method == 'POST':
        form = CargoForm(request.POST,instance=cargo)
        if form.is_valid():
            form.save()
            #REDIRIGIR A EMPLEADOS INDEX
            cargos = Cargo.objects.select_related()
            return render_to_response('cargo_templates/cargo_index.html',{'cargos':cargos}, context_instance=RequestContext(request))
           # return redirect('empleado_index',empleados)
    else:
        form = CargoForm(instance=cargo)
    return render_to_response('cargo_templates/cargo_editar.html',{'form':form},context_instance = RequestContext(request))

def get_nombre_cargo(request,pkey):
    nombre_cargo = Cargo.objects.get(pk=pkey)
    return nombre_cargo.cargo


@login_required
def area_crear(request):
    if request.method == 'POST': #desde aca va para la persistencia de datos
        form = AreaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('areas')
    else:
        form = AreaForm()
    return render_to_response('area_templates/area_crear.html',
                                {'form': form},
                                context_instance=RequestContext(request))
@login_required
def area_editar(request, area_id):
    area = get_object_or_404(Area, pk=area_id)
    if request.method == 'POST':
        form = AreaForm(request.POST,instance=area)
        if form.is_valid():
            form.save()
            #REDIRIGIR A AREAS INDEX
            areas = Area.objects.select_related()
            return render_to_response('area_templates/area_index.html',{'areas':areas})
    else:
        form = AreaForm(instance=area)
    return render_to_response('area_templates/area_editar.html',{'form':form},
                                  context_instance = RequestContext(request))
