from django.conf.urls import patterns,include, url #include para la pagina admin
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','personalmanager.views.homepage',name="homepage"),
    url(r'^empleados/$','personalmanager.views.empleado_index',name='empleados'),
    url(r'^cargos/$','personalmanager.views.cargo_index',name='cargos'),
    url(r'^areas/$','personalmanager.views.area_index',name='areas'),
    url(r'^empleados/crear/$','personalmanager.views.empleado_crear',name='empleado_crear'),
    url(r'^empleados/editar/(?P<empleado_id>\d+)/$','personalmanager.views.empleado_editar', name='empleado_editar'),
    url(r'^empleados/eliminar/(?P<empleado_id>\d+)/$','personalmanager.views.empleado_eliminar', name='empleado_eliminar'),
    url(r'^cargos/crear/$','personalmanager.views.cargo_crear',name='cargo_crear'),
    url(r'^cargos/editar/(?P<cargo_id>\d+)/$','personalmanager.views.cargo_editar', name='cargo_editar'),
    url(r'^areas/crear/$','personalmanager.views.area_crear',name='area_crear'),
    url(r'^areas/editar/(?P<area_id>\d+)/$','personalmanager.views.area_editar', name='area_editar'),
    url(r'^login/$','sistemapersonal.views.login_page',name='login'),
    url(r'^logout/$','sistemapersonal.views.logout_view',name='logout'),


)
